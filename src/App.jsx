import { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import storage from './services/storage';
import { Navbar, MainList, Completed } from './components';
import './App.scss';

const INITIAL_TODOS = storage.getItem('todos') || [];
const INITIAL_COMPLETED = storage.getItem('completed') || [];

const App = () => {
  const [todos, setTodos] = useState(INITIAL_TODOS);
  const [completed, setCompleted] = useState(INITIAL_COMPLETED);
  const [navbarInputValue, setNavbarInputValue] = useState('');

  const completeTodo = (todoId) => {
    //1. Buscar el elemento en el array de todos.
    const todo = todos.find((t) => t.id === todoId);
    //2. Añadir ese elemento a completed.
    setCompleted([...completed, todo]);
    storage.setItem('completed', [...completed, todo]);
    //3. Eliminar el elemento de todos. 
    const newTodos = todos.filter((t) => t.id !== todoId);
    setTodos(newTodos);
    storage.setItem('todos', newTodos);
  }

  const uncompleteTodo = todoId => {
    //1. Buscar el elemento en el array de completed.
    const todo = completed.find((t) => t.id === todoId);
    //2. Añadir ese elemento a todos.
    setTodos([...todos, todo]);
    storage.getItem('todos', [...todos, todo]);
    //3. Eliminar el elemento de completed. 
    const newTodos = completed.filter((t) => t.id !== todoId);
    setCompleted(newTodos);
    storage.setItem('completed', newTodos);
  }

  const newTask = (task) => {
    const taskToAdd = {
      title: task,
      id: uuidv4(),
    };
    setTodos([...todos, taskToAdd]);
    storage.setItem('todos', [...todos, taskToAdd] );
  }

  const filterTodos = () => {
    if (navbarInputValue) {
      //que filtre y devuelva lo filtrado
      return todos.filter(todo => todo.title.toLocaleLowerCase().includes(navbarInputValue.toLocaleLowerCase()));
    } else {
      //Si no tengo texto para buscar devuelvo todos
      return todos
    }
  }

  const handleNabvarInput = (value) => {
    setNavbarInputValue(value);
  }

  return (
    <Router>
      <div className="App">
        <Navbar changeNavbarInput={handleNabvarInput} navbarInputValue={navbarInputValue} />
        <Switch>
          <Route exact path="/" component={(props) => <MainList
            {...props}
            todos={filterTodos()}
            handleTodo={completeTodo}
            newTask={newTask} />} />
          <Route exact path="/completed" component={(props) => <Completed
            {...props}
            completed={completed}
            handleTodo={uncompleteTodo} />} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
