import { Link } from  'react-router-dom';
import './Navbar.scss';




const Navbar = (props) => {

    const handleInput = (ev) => {
        const {value} = ev.target;
        props.handleNabvarInput(value);
    }
    return (
        <nav className="nav">
            <div className="nav__left">
                <ul>
                    <Link to="/">Home</Link>
                    <li><input type="text" onChange={handleInput} value={props.navbarInputValue}/></li>
                </ul>
            </div>
            <div className="nav__right">
                <ul>
                    <li>+</li>
                    <Link to="/completed">Tareas Completadas</Link>
                </ul>
            </div>
        </nav>
    )
}

export default Navbar;