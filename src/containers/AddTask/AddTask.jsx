import { useState } from 'react';
import './AddTask.scss';

const AddTask = (props) => {
    const [openFrom, setOpenForm] = useState(false);
    const [task, setTask] = useState('');

    const onCreateTask = (ev) => {
        ev.preventDefault();
        if(task) {
            props.newTask(task);
            setTask('');
        }
    }

    const handleOpenForm = () => {
        setOpenForm(!openFrom);
        setTask('');
    }

    return (
        <div className="add-task">
            {!openFrom && <div className="add-task__text" onClick={handleOpenForm}>
                <span className="add-task__icon">+</span>
            Añadir Tarea
            </div>}

            {openFrom && <div>
                <form onSubmit={onCreateTask}>
                    <div>
                        <input type="text" onChange={(ev) => setTask(ev.target.value)} value={task} />
                    </div>
                    <div>
                        <button type="submit">Add Task</button>
                        <button onClick={handleOpenForm}>Cancel</button>
                    </div>
                </form>
            </div>}
        </div>
    )
}

export default AddTask;